Parallel and Multicore Computing - Project 2
============================================

This repository contains the code of the project 2 in the subject Parallel and Multicore Computing. As part of the
project, we implemented parallel algorithms for detecting edges in images. Edge detection is an important part in
several areas.

Utilizing OpenMP and MPI

