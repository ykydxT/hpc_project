\documentclass[a4paper]{article}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}
\usepackage{graphicx}
\usepackage{algorithm2e}
\usepackage{cite}
\usepackage{amsmath}

\begin{document}
\title{Parallel and Multicore Computing - COMP90025 \\
Project 2 - Parallelisation of Edge Detection}

\author{Rabindra Kumar Panda \\
The University of Melbourne \\
Email: rpanda@student.unimelb.edu.au \\
Student ID: 643998 \\
Login Name: rpanda
\and
Dexiao Ye \\
The University of Melbourne \\
Email: dexiaoy@student.unimelb.edu.au \\
Student ID: 827499 \\
Login Name: dexiaoy}

\maketitle

\begin{abstract}
In this report we present the details of our project in which we implemented parallel algorithms for edge detection in
image segmentation problem. The parallel algorithms were implemented using OpenMP and MPI platforms. We were able to
obtain significant speedup by our parallel edge detection programs. The details of the algorithm, design of the parallel
algorithms and the results of this experiment are outlined in this report.
\end{abstract}

\section{Introduction}
The aim of edge detection is to locate the edges in an image. This is a very important task that enables us to further
discover important features in an image. An edge in an image is detected when there is a significant local change in
image intensity. This change is usually a discontinuity in intensity or its first derivative. Further this discontinuity
can be either a step discontinuity or a line discontinuity. In step discontinuity the intensity suddenly changes from
one value to the other along the discontinuity, while in line discontinuity the intensity changes to a different value
but returns to the starting value within short distance. There are several algorithms for edge detection. One of the most
common algorithm is to use a filter window of surrounding pixels and convolve it for current pixel to determine whether
the pixel is part of an edge or not. In this project we will implement the Sobel edge detection algorithm. An example
of edge detection is shown in figure \ref{fig:edge_detection}.

The edge detection algorithm is computationally expensive but the performance of the algorithm can significantly be reduced
by using parallel algorithms. As we will see in further sections this parallelisation can be easily done by processing
individual elements of the image in parallel. The primitive task for computing an edge is the same for both the sequential
and parallel algorithms.

The rest of the paper is organised into sections and is as follow. In section II we present a sequential Sobel algorithm
for detecting edges in an image. In section III we present a parallel implementation using the OpenMP library. In section
IV we redesign our parallel algorithm to use the MPI library. We present the results of our experiments and perform a
performance analysis in section V.

\begin{figure}[!ht]
\centering
  \includegraphics[width=0.30\textwidth,height=\textheight,keepaspectratio]{figures/lena.jpg}
  \includegraphics[width=0.30\textwidth,height=\textheight,keepaspectratio]{figures/lena-edge.jpg}
  \caption{Example of applying edge detection to an image}
  \label{fig:edge_detection}
\end{figure}

\section{Edge detection algorithm}
Let's consider a grayscale $m\times n$ image, with m rows and n columns. Each pixel (x,y) has an intensity value between
0 and 255. So the input image can be represented as a two dimensional matrix. The idea of the Sobel algorithm is to
compute the gradient vector at each pixel of the image, which then determines whether or not the edge is part of an edge.
The sequential algorithm is presented in Algorithm \ref{algo_edgedetection}.

\begin{algorithm}
    \SetKwInOut{Input}{Input}
    \SetKwInOut{Output}{Output}
    \caption{Sequential Edge Detection Algorithm}
    \Input{A gray-scale image of size $m\times n$, represented in matrix $I(x,y)$, $0\leq x \leq m-1$ and
           $0\leq y \leq n-1$}
    \Output{A binary image of size $m\times n$ with black and white intensities, represented in a matrix $O(x,y)$}
    \BlankLine
    \For{$i\leftarrow 1$ \KwTo $m-2$}{
      \For{$j\leftarrow 1$ \KwTo $n-2$}{\nllabel{forins}
          At pixel $I(i,j)$, calulate the horizontal and vertical
          approximations of the gradients $S_x$ and $S_y$, using equations \eqref{eq:Sx}
          and \eqref{eq:Sy} respectively.
          \BlankLine
          Find the approximated net magnitude of the gradient vector at pixel $I(i,j)$ using
          equation \eqref{eq:netGradient}.
      }
    }
    \label{algo_edgedetection}
\end{algorithm}

There are two $3\times 3$ matrices, known as Sobel edge detectors, that are used to convolve with the pixel $I(x,y)$
along the horizontal and vertical axes. These mask matrices are given in equations \eqref{eq:Mx} and \eqref{eq:My}
respectively. The mask matrix $M_x$ is used to detect edges along the x-axis while the matrix $M_Y$ is used to detect
edges along the y-axis.

\begin{equation}\label{eq:Mx}
 M_x = \begin{bmatrix}
         -1 & 0 & 1 \\[0.3em]
         -2 & 0 & 2 \\[0.3em]
         -1 & 0 & 1
        \end{bmatrix}
\end{equation}

\begin{equation}\label{eq:My}
 M_y = \begin{bmatrix}
         -1 & -2 & -1 \\[0.3em]
          0 & 0 & 0 \\[0.3em]
          1 & 2 & 1
        \end{bmatrix}
\end{equation}

For every pixel $I(x,y)$, the Sobel algorithm calculates the weighted values $S_x(x,y)$ and $S_y(x,y)$ using the
equations \eqref{eq:Sx} and \eqref{eq:Sy}. The resulting values are used to calculate the approximate gradient at the
pixel.

\begin{equation}\label{eq:Sx}
 S_x(x,y) = \sum_{i=-1}^{1} \sum_{j=-1}^{1} W_x(i,j)I(x+i, y+j)
\end{equation}

where $W_x(i,j)$ is the weight in the Sobel mask $M_x(i+1, j+1)$

\begin{equation}\label{eq:Sy}
 S_x(x,y) = \sum_{i=-1}^{1} \sum_{j=-1}^{1} W_y(i,j)I(x+i, y+j)
\end{equation}

where $W_y(i,j)$ is the weight in the Sobel mask matrix $M_y(i+1, j+1)$


The net magnitude of the gradient vector at each pixel is approximated and can be calculated using the equation
\label{eq:netGradient}. Each pixel in the resulting output image has black and white intensities. The white intensity
represents an edge while the black intensity represents the background.

\begin{equation}\label{eq:netGradient}
  \lvert \nabla f\rvert \approx \lvert S_x\rvert + \lvert S_y\rvert
\end{equation}

\section{Parallelisation}
The edge detection problem is inherently a parallel problem and the edges in the whole image can be determined in
parallel. The operations involved can be applied to all the pixels at the same time. There is one thing to consider
for the parallel algorithm, which is the data dependencies. In Sobel algorithm, calculating the net gradient for a
pixel depends on eight neighbour pixels. However the required neighbour values are the initial values and not the
updates ones. Hence, using a good partitioning scheme we can handle the data dependency.

We used two of the most popular parallel programming paradigms - MPI and OpenMP for implementing the parallel algorithms.
In the following two sub sections we present the details of how we designed our algorithm for each of these implementations.

\subsection{MPI}
The choice of using MPI to solve such a parallel problem as edge detection is natural, as MPI enables the programmer a
finer control on distributing data and synchronising processes.
The parallel edge detection algorithm for MPI was designed in a master/slave pattern. While designing the parallel
algorithm for MPI, we followed a step by step process. In the following sections we briefly describe these steps -
partitioning, communication, agglomeration and mapping.

\subsubsection{Partitioning}
During partitioning, the problem is solved by dividing the data or the computation. The two options of partitioning are hence
known as domain and functional decomposition respectively. In domain decomposition the data is divided into multiple
pieces and the processes perform the same primitive task in parallel on those pieces. On the other hand during functional
decomposition, the computation is divides into small sub computations and the processes perform these different sub tasks
in parallel on the same data.

The edge detection problem falls under the category of domain decomposition, as individual pixels can be processed
independently. The primitive task that each of the processes needs to perform is to apply the Sobel operator on the
pixel to determine the gradient.

\subsubsection{Communication}
In parallel algorithm design two types of communication patterns can be selected, local and global. In local communication
a minimal interaction is required among the processes, while in global a large number of interactions exist. In our
parallel design for the MPI implementation, local communication is used. The master processor distributes the original image
data to all processors using the MPI\_Scatterv operation. All processors perform the same primitive task, the Sobel operator, on the
pixels of their local part of the image. In the end the master processor collects the results using the MPI\_Gather operation
and writes the result to the output image.

\subsubsection{Agglomeration}
The aim of agglomeration is to group the primitive tasks to increase performance. Instead of each processor taking a single
pixel, we used row-wise block-stripped decomposition of the image data, as it is easier to distribute the rows among processors
and gather the results, especially in C/C++ programming languges. Each processor is assigned a sequence of rows as
block. The number of rows is determined by the image height. If the number of rows are not evenly distributed among the
processors, some processors have one extra row to work on.

\subsubsection{Mapping}
The goal of mapping is to reduce the execution time. In this phase we assign the individual tasks to the The task are
divided among the processors and the computations are
the same. To reduce communication, the tasks assigned to each processor do not need frequent communication from tasks
running on other processors.

\subsection{OpenMP}
We used the standard OpenMP library for the multithreaded parallel program. Different from the MPI program, the OpenMP
program processes the image column by column.

Figure \ref{fig:omp_method} shows how the parallel strategy in our OpenMP works and it is temporarily assumed that
the program is executed by four threads. The first column is executed by all the threads and there is a barrier to make
sure all the threads finish their tasking before going to the next parallel region. Then, in the main parallel region,
the rest columns except the last column are statically and equally assigned to the four threads. If there are any
columns left (because of the division issue), these columns along with the last column are executed by all the threads
in the last parallel region.

\begin{figure}[!ht]
\centering
  \includegraphics[width=0.50\textwidth,height=\textheight,keepaspectratio]{figures/omp_method.png}
  \caption{Parallel strategy in our OpenMP implementation with four threads.}
  \label{fig:omp_method}
\end{figure}

The reason of processing the first and last column separately from other columns is that the workload of these two
columns are less than other columns. The workload on each pixel is same except the pixels on the border. Because part
of the Sobel matrixes of the pixels on the border is directly set to zero without any other operation, which causes
unequal workload. Without the first and the last column, other columns are divided into blocks and the workload of each
block is same. With the same workload, threads can finish their own workload at roughly same time and it reduces the
time that one thread waiting for other threads. Besides, since the threads work on their own block statically, the cost
of assignment is minimised.

\section{Performance Analysis}
In this section we outline the experimental setup and the results of the Sequential, OpenMP and MPI programs. We evaluate
the speedup and efficiency of the parallel programs.

\subsection{Experimental setup}
We ran the programs on the VLSCI systems.We used the Snowy system which is a Lenovo NeXtScale x86 system. This system has
992 Intel Xeon E5-2698 v3 cores running at 2.3GHz. The system runs the RHEL 6 operating system. The GCC compiler suite
for running the OpenMP and MPI programs were used. For evaluation of the program execution times we selected three
bitmap images of varying sizes.

\subsection{Results}

\begin{table}
\centering
    \begin{tabular}{c}
        \includegraphics[width=0.8\textwidth,height=\textheight,keepaspectratio]{figures/mpi-results.pdf}
    \end{tabular}
    \caption[MPI Results on small image]{Performance results of the MPI implementation of the edge detection
    algorithm on images of different sizes}
    \label{table:mpi_results}
\end{table}

\begin{table}
\centering
    \begin{tabular}{c}
        \includegraphics[width=0.8\textwidth,height=\textheight,keepaspectratio]{figures/omp-results.pdf}
    \end{tabular}
    \caption[OpenMP Results on small image]{Performance results of the OpenMP implementation of the edge detection
    algorithm on images of different sizes}
    \label{table:openmp_results}
\end{table}

The expectation of a parallel algorithm design is to gain improved speed up in execution time. Speed up is defined by
the ratio of execution time of sequential program and execution time of parallel program. Another measure that is important
is efficiency, which is the speed up divided by the number of processors used. In tables \ref{table:mpi_results} and
\ref{table:openmp_results}, we present the results of the MPI and OpenMP implementation of the parallel edge detection
algorithms.
As we can see the speedup of the parallel algorithm is increased as we increase the number of processors until it reaches
a saturation point.
The efficiency is however decreases as we increase the number of processors, as the increase in speedup is not linear
to the number of processors. This is because the number of tasks performed by each processor is reduced and communication
cost burden is increased. This behaviour is not unexpected as for same size image data the increase in processors after
a particular number reduce the utilisation of the processors. This also explains why for small sized images the increase
in the number of processors has negative effect on speedup.

The effect of number of processors on the performance of the MPI program is illustrated in figure \ref{fig:mpi_graphs}.
Similarly in figure \ref{fig:omp_graphs}, the relationship between the number of threads with speedup and efficiency is
shown.

\begin{figure}[!ht]
\centering
  \includegraphics[width=0.44\textwidth,height=\textheight,keepaspectratio]{figures/mpi_speedup.pdf}
  \includegraphics[width=0.44\textwidth,height=\textheight,keepaspectratio]{figures/mpi_efficiency.pdf}
  \caption{Speedup and efficiency graphs for MPI parallel edge detection.}
  \label{fig:mpi_graphs}
\end{figure}

\begin{figure}[!ht]
\centering
  \includegraphics[width=0.44\textwidth,height=\textheight,keepaspectratio]{figures/omp_speedup.pdf}
  \includegraphics[width=0.44\textwidth,height=\textheight,keepaspectratio]{figures/omp_efficiency.pdf}
  \caption{Speedup and efficiency graphs for OpenMP parallel edge detection.}
  \label{fig:omp_graphs}
\end{figure}


\section{Conclusion}
In this paper, we presented parallel implementations of the Sobel's edge detection algorithm. We devised two different
strategies for implementing the parallel algorithm using OpenMP and MPI. For our experiment, we first designed a
sequential algorithm and modified it to our parallelisation strategies. Doing so, we obtained significant improvement
in speedup. In the MPI implementation The parallelisation algorithms distributed data across the number of processors
as they are all performing the same sequential code on every pixel but for parts of the image data. Similarly in our
OpenMP implementation the threads are performing the same primitive task on different columns of the image data in
parallel.

The results we obtained for the parallel algorithms were reasonably better than the sequential version of the algorithm.
We observed how increasing the number of processors in MPI and number of threads in OpenMP implementations, affected the
overall speedup and efficiency of the parallel algorithms.

\bibliographystyle{apalike}
\bibliography{project2}
\nocite{*}

\end{document}