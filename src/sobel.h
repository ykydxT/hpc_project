/*
 * The Sobel edge detection operator function. This function is used by all
 * the three versions (Sequential, MPI and OpenMP)
 *
 * Authors: Dexiao Ye
 *          Rabindra Panda
 *
 * Date: 17/10/2016
 */
#ifndef PMC_PROJECT2_SOBEL_H
#define PMC_PROJECT2_SOBEL_H

#include "include/bitmap_image.hpp"

#define CLAMP(x)  (((x) > (255)) ? (255) : (((x) < (0)) ? (0) : (x)))

typedef struct Pixel {
    unsigned char B;
    unsigned char G;
    unsigned char R;
} Pixel;

int M_x[3][3] = {{-1, 0, 1},
                 {-2, 0, 2},
                 {-1, 0, 1}};
int M_y[3][3] = {{-1, -2, -1},
                 {0,  0,  0},
                 {1,  2,  1}};

Pixel sobel_edge(unsigned int x, unsigned int y, bitmap_image image);

Pixel sobel_edge(unsigned int x, unsigned int y, bitmap_image image) {

    Pixel segment[3][3];
    Pixel pixel;
    int x_matrix[3][3], y_matrix[3][3];
    int x_red = 0, y_red = 0;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            //if the pixel is at the border
            if ((y == 0 && i == 0) || (y == (image.height() - 1) && i == 2)
                || (x == 0 && j == 0) || (x == (image.width() - 1) && (j == 2))) {
                segment[i][j].R = segment[i][j].G = segment[i][j].B = 0;
            } else {
                image.get_pixel((x - 1 + j), (y - 1 + i), segment[i][j].R, segment[i][j].G, segment[i][j].B);
            }

            x_matrix[i][j] = M_x[i][j] * segment[i][j].R;
            y_matrix[i][j] = M_y[i][j] * segment[i][j].R;
            x_red = x_red + x_matrix[i][j];
            y_red = y_red + y_matrix[i][j];
        }
    }

    unsigned char gradient = (unsigned char) CLAMP(sqrt(pow(x_red, 2) + pow(y_red, 2)));

    pixel.R = pixel.G = pixel.B = gradient;

    return pixel;
}

#endif
