/*
 * MPI Implementation of Sobel edge detection
 *
 * Authors: Dexiao Ye
 *          Rabindra Panda
 *
 * Date: 17/10/2016
 */
#include <cstdlib>
#include <cstdio>
#include "mpi.h"
#include "include/bitmap_image.hpp"
#include "sobel.h"

int main(int argc, char *argv[]) {
    int my_rank, nprocs;
    unsigned int width, height;
    int bytes_per_pixel;
    double start_time = 0;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    unsigned char *image_data = NULL;
    // If process rank is 0, then read the image from file
    // and copy the image data into local variable for
    // scattering it among all processes
    if (my_rank == 0) {
        start_time = MPI_Wtime();
        bitmap_image image(argv[1]);
        if (!image) {
            printf("Falied to open bmp file!!! \n");
            return EXIT_FAILURE;
        }
        image.convert_to_grayscale();

        height = image.height();
        width = image.width();
        bytes_per_pixel = image.bytes_per_pixel();
        image_data = new unsigned char[width * height * bytes_per_pixel];
        memcpy(image_data, image.data(), sizeof(unsigned char) * width * height * bytes_per_pixel);
    }

    // Broadcast image size details
    MPI_Bcast(&height, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&width, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&bytes_per_pixel, 1, MPI_INT, 0, MPI_COMM_WORLD);

    // Distribution size and start indexes for the participating
    // processes.
    int *dist_start, *dist_size;

    dist_size = new int[nprocs];
    dist_start = new int[nprocs];
    unsigned int row_size = width * bytes_per_pixel;

    for (int p = 0; p < nprocs; p++) {
        if (height % nprocs > p) {
            dist_size[p] = (height / nprocs + 1) * row_size;
        } else {
            dist_size[p] = (height / nprocs) * row_size;
        }
        if (p == 0) {
            dist_start[p] = 0;
        } else {
            dist_start[p] = dist_start[p - 1] + dist_size[p - 1];
        }
    }

    // As the for the Sobel edge operator for a pixel requires the eight
    // neighbour pixels, we add two (one for first and last process) more
    // rows (previous and next) for reference.
    for (int p = 0; p < nprocs; p++) {
        if (p == 0 || p == nprocs - 1) {
            dist_size[p] += row_size;
        } else {
            dist_size[p] += (2 * row_size);
        }
        if (p != 0) {
            dist_start[p] -= row_size;
        }
    }

    unsigned char *local_dist = new unsigned char[dist_size[my_rank]];

    // Scatter the image data
    MPI_Scatterv(image_data, dist_size, dist_start, MPI_UNSIGNED_CHAR, local_dist, dist_size[my_rank],
                 MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD);

    // This is the reference segment of the image needed for the process.
    bitmap_image ref_image_segment(width, dist_size[my_rank] / row_size);
    ref_image_segment.set_data(local_dist);

    // Reset the distribution size for processing
    if (my_rank == 0 || my_rank == nprocs - 1) {
        dist_size[my_rank] -= row_size;
    } else {
        dist_size[my_rank] -= (2 * row_size);
    }

    unsigned int num_rows = dist_size[my_rank] / row_size;
    // This is the resulting segment of the edge image
    bitmap_image edge_image_segment(width, num_rows);

    Pixel pixel;
    // Process one start at row zero of its reference distribution size
    // others start at the first record.
    unsigned int start_row = my_rank == 0 ? 0 : 1;

    for (unsigned int x = start_row; x < start_row + num_rows; x++) {
        for (unsigned int y = 0; y < width; y++) {
            pixel = sobel_edge(y, x, ref_image_segment);
            edge_image_segment.set_pixel(y, x - start_row, pixel.R, pixel.G, pixel.B);
        }
    }

    // Gather edge image segments here.
    unsigned char *edge_image = new unsigned char[width * height * bytes_per_pixel];
    MPI_Gather(edge_image_segment.data(), dist_size[my_rank], MPI_UNSIGNED_CHAR, edge_image, dist_size[my_rank],
               MPI_UNSIGNED_CHAR, 0,
               MPI_COMM_WORLD);

    if (my_rank == 0) {
        bitmap_image result(width, height);
        result.set_data(edge_image);
        result.save_image("edges_mpi.bmp");

        printf("Total runtime is %lf seconds\n", MPI_Wtime() - start_time);
    }

    MPI_Finalize();
    return EXIT_SUCCESS;
}

