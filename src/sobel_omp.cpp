/*
 * OpenMP implementation of the edge detection algorithm.
 *
 * Authors: Dexiao Ye
 *          Rabindra Panda
 *
 * Date: 17/10/2016
 */
#include <cstdio>
#include <cstdlib>
#include "include/bitmap_image.hpp"
#include "sobel.h"
#include "omp.h"

int main(int argc, char *argv[]) {
    double start_t, end_t;
    start_t = omp_get_wtime();
    Pixel single_pixel;
    for (int s = 1; s < 2; s++) {
        bitmap_image image(argv[s]);
        if (!image) {
            printf("Falied to open bmp file!!! \n");
            return EXIT_FAILURE;
        }
        unsigned int height = image.height();
        unsigned int width = image.width();
        unsigned int pixel_num = height * width;

        image.convert_to_grayscale();

        bitmap_image result(width, height);
        unsigned int num = omp_get_max_threads();
        int chunk_1 = width / num;
        printf("Number of threads = %d\n", num);
        for (unsigned int y = 0; y < width; y++) {
            single_pixel = sobel_edge(y, 0, image);
            result.set_pixel(y, 0, single_pixel.R, single_pixel.G, single_pixel.B);
        }
#pragma omp barrier
        num = omp_get_max_threads();
        unsigned int chunk = (height - 2) / num;
        unsigned int first = num * chunk;
        unsigned int rest = height - first - 2;
#pragma omp parallel for collapse(2) schedule(static, chunk) private(single_pixel) firstprivate(image) shared(result)
        for (unsigned int x = 1; x < first; x++) {
            for (unsigned int y = 0; y < width; y++) {
                single_pixel = sobel_edge(y, x, image);
                result.set_pixel(y, x, single_pixel.R, single_pixel.G, single_pixel.B);
            }
        }
        if (rest != 0) {
#pragma omp parallel for private(single_pixel) firstprivate(image) shared(result)
            for (unsigned int x = first; x < height - 1; x++) {
                for (unsigned int y = 0; y < width; y++) {
                    single_pixel = sobel_edge(y, x, image);
                    result.set_pixel(y, x, single_pixel.R, single_pixel.G, single_pixel.B);
                }
            }
        }
#pragma omp parallel for private(single_pixel) firstprivate(image) shared(result)
        for (int y = 0; y < width; y++) {
            single_pixel = sobel_edge(height - 1, 0, image);
            result.set_pixel(height - 1, 0, single_pixel.R, single_pixel.G, single_pixel.B);
        }

        result.save_image("edges_omp.bmp");
        end_t = omp_get_wtime();
        printf("Total runtime is %f seconds\n", end_t - start_t);
    }
    return EXIT_SUCCESS;
}
