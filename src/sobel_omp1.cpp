//
// Created by Dexiao Ye on 17/10/2016.
//

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "include/bitmap_image.hpp"
#include "sobel.h"


int main(int argc, char *argv[]) {
    omp_set_num_threads(4);
    double start_t, end_t;
    start_t = omp_get_wtime();
    Pixel single_pixel;
    for (int s = 1; s < 2; s++) {
        bitmap_image image(argv[s]);
        if (!image) {
            printf("Falied to open bmp file!!! \n");
            return 1;
        }
        int height = image.height();
        int width = image.width();
        int pixel_num = height * width;

        image.convert_to_grayscale(); //covert the image to grayscale

        bitmap_image result(width, height);  // the output image
        int num = omp_get_max_threads();
        int chunk_1 = width / num;
        printf("%d\n", num);
        // #pragma omp parallel for private(single_pixel)firstprivate(image)shared(result)
        for (int y = 0; y < width; y++) {
            single_pixel = sobel_edge(y, 0, image);
            result.set_pixel(y, 0, single_pixel.R, single_pixel.G, single_pixel.B);
        }
#pragma omp barrier

        num = omp_get_max_threads();
        int chunk = (height - 2) / num;
        int first = num * chunk;
        int rest = height - first - 2;
        printf("%d\n", first);
        printf("rest: %d\n", rest);
#pragma omp parallel for collapse(2) schedule(static, chunk) private(single_pixel) firstprivate(image) shared(result)
        for (int x = 1; x < first; x++) {
            for (int y = 0; y < width; y++) {
                single_pixel = sobel_edge(y, x, image);
                result.set_pixel(y, x, single_pixel.R, single_pixel.G, single_pixel.B);
            }
        }
        //#pragma omp barriers
        if (rest != 0) {
#pragma omp parallel for private(single_pixel) firstprivate(image) shared(result)
            for (int x = first; x < height - 1; x++) {
                for (int y = 0; y < width; y++) {
                    single_pixel = sobel_edge(y, x, image);
                    result.set_pixel(y, x, single_pixel.R, single_pixel.G, single_pixel.B);
                }
            }
        }
        printf("%d\n", width);
#pragma omp parallel for private(single_pixel) firstprivate(image) shared(result)
        for (int y = 0; y < width; y++) {
            single_pixel = sobel_edge(height - 1, 0, image);
            result.set_pixel(height - 1, 0, single_pixel.R, single_pixel.G, single_pixel.B);
        }

        result.save_image("result.bmp");
        end_t = omp_get_wtime();
        printf("runtime: %f", end_t - start_t);

    }

    return 0;
}
