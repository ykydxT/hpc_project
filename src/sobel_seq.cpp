/*
 * A Sequential version of the edge detection algorithm
 * using Sobel's algorithm.
 *
 * Authors: Dexiao Ye
 *          Rabindra Panda
 *
 * Date: 17/10/2016
 */

#include <cstdio>
#include <cstdlib>
#include <time.h>
#include "include/bitmap_image.hpp"
#include "sobel.h"

int main(int argc, char *argv[]) {
    clock_t start, end;
    start = clock();
    Pixel pixel;
    bitmap_image image(argv[1]);
    if (!image) {
        printf("Failed to open bmp image file. \n");
        return EXIT_FAILURE;
    }
    const unsigned int height = image.height();
    const unsigned int width = image.width();

    image.convert_to_grayscale();

    bitmap_image result(height, width);

    for (unsigned int x = 0; x < height; x++) {
        for (unsigned int y = 0; y < width; y++) {
            pixel = sobel_edge(x, y, image);
            result.set_pixel(x, y, pixel.R, pixel.G, pixel.B);
        }
    }

    result.save_image("edges_seq.bmp");
    end = clock();
    printf("Sequential runtime is %f seconds\n", (double) (end - start) / CLOCKS_PER_SEC);
    return EXIT_SUCCESS;
}